import { getRandomNumber } from './helpers/getRandomNumber';


export function fight(firstFighter, secondFighter) {
    let round = 1;
    let fighter = [];
    let fighting = true;

    let f1_name = firstFighter.name;
    let f1_health = firstFighter.health;
    let f1_image = firstFighter.source;

    let f2_name = secondFighter.name;
    let f2_health = secondFighter.health;
    let f2_image = firstFighter.source;

    let f1_defence = getBlockPower(firstFighter);
    let f1_attack = getHitPower(firstFighter);

    let f2_defence = getBlockPower(secondFighter);
    let f2_attack = getHitPower(secondFighter);

    while (fighting) {
        if (fighting) {
            f2_health = f2_health + f2_defence - f1_attack;
            if (f2_health <= 0) {
                fighting = false;
                fighter = [f1_image, f1_name];
            }
        }

        if (fighting) {
            f1_health = f1_health + f1_defence - f2_attack;
            if (f1_health <= 0) {
                fighting = false;
                fighter = [f2_image, f2_name];
            }
        }
        round++;
    }
    return fighter;
}


export function getDamage(attacker, enemy) {
    const damage = getHitPower(attacker) - getBlockPower(enemy);
    return damage > 0 ? damage : 0
}

export function getHitPower(fighter) {
    const criticalHitChance = getRandomNumber(1, 2);
    return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
    const dodgeChance = getRandomNumber(1, 2);
    return fighter.defense * dodgeChance;
}