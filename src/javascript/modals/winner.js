import { showModal } from './modal';

export function showWinnerModal(fighter) {

    let fighterImage = "<img src=" + fighter[0] + " >";
    let fighterImg = document.createElement('div');
    fighterImg.innerHTML = fighterImage;

    const imageElement = fighterImg;
    const modalElement = {
        title: `${fighter[1]} WINNER!!!`,
        bodyElement: imageElement,
        onClose: () => {
            location.reload();
        }
    };


    showModal(modalElement);
}