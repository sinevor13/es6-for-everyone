import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export function showFighterDetailsModal(fighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterDetails(fighter);
    showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
    const { name, health, attack, defense } = fighter;

    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = createElement({ tagName: 'h2', className: 'fighter-name' });
    const attackElement = createElement({ tagName: 'p', className: 'fighter-attack' });
    const defenseElement = createElement({ tagName: 'p', className: 'fighter-defense' });
    const healthElement = createElement({ tagName: 'p', className: 'fighter-health' });
    const imageElement = createFighterImage(fighter);

    nameElement.innerText = name;
    attackElement.innerText = attack;
    defenseElement.innerText = defense;
    healthElement.innerText = health;
    fighterDetails.append(imageElement, nameElement, attackElement, defenseElement, healthElement);
    return fighterDetails;
}

export function createFighterImage(fighter) {
    const { source, name } = fighter;
    const attributes = {
        src: source,
        title: name,
        alt: name
    };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter-image',
        attributes,
    });

    return imgElement;
}